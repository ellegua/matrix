package com.company;

public class TestPassed {
    private String msg;

    public TestPassed(String msg){
        this.msg=msg;
    }

    public String toString() {
        return "[PASSED] " + msg ;
    }
}
