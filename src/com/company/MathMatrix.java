package com.company;

import com.company.Exceptions.*;

import java.util.Scanner;

public class MathMatrix { // TODO Create static member array to store matrix
    private IMath type;
    private int row;
    private int col;
    private IMath m[][];

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public IMath getType() {
        return type;
    }

    public IMath getData(int row, int col) {
        if (row < 0 || col < 0 || row >= this.row || col >= this.col) {
            throw new OutOfDimension(row, col, this);
        }
        return m[row][col];
    }

    public void setData(int row, int col, IMath value) {
        if (row < 0 || col < 0 || row >= this.row || col >= this.col) {
            throw new OutOfDimension(row, col, this);
        }
        m[row][col] = value;
    }

    public MathMatrix(IMath type, int row, int col) {
        if (row <= 0 || col <= 0) {
            throw new NullDimension(row, col);
        }

        this.type = type;
        this.row = row;
        this.col = col;
        m = new IMath[row][col];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                m[i][j] = type.copy();
            }
        }
    }

    public MathMatrix(IMath type) {
        this(type, 1, 1);
    }

    public MathMatrix() {
        this(new MathComplex(), 1, 1); // Hard-code
    }

    public String toString() {
        String s = "";
        for (int i = 0; i < row; i++) {
            s += "( ";
            for (int j = 0; j < col; j++) {
                s += m[i][j].toString() + " ";
            }
            s += ")" + String.format("%n"); //is OS independent
        }
        return s;
    }

    public static MathMatrix createFromConsole(IMath type) {
        Scanner in = new Scanner(System.in);
        System.out.println("Input Matrix:");
        System.out.println("Input row col:");
        MathMatrix cM = new MathMatrix(type.copy(), in.nextInt(), in.nextInt()); // Hard-code !
        System.out.println("Input Data:");

        for (int i = 0; i < cM.getRow(); i++) {
            for (int j = 0; j < cM.getCol(); j++) {
                System.out.print("z[");
                System.out.print(i);
                System.out.print(",");
                System.out.print(j);
                System.out.print("]=");
                cM.setData(i, j, cM.getType().createFromConsole());
            }
        }
        return cM;
    } // TODO Think about separated class or interface to work with console

    public static MathMatrix createFromArray(IMath type, double staff[][][]) {
        if (staff.length == 0 || staff[0].length == 0) {
            throw new NullArray();
        }
        MathMatrix cM = new MathMatrix(type, staff.length, staff[0].length); // Generate exception if staff.length or staff[0].length
        for (int i = 0; i < staff.length; i++) {
            for (int j = 0; j < staff[0].length; j++) {
                cM.setData(i, j, cM.getType().createFromArray(staff[i][j])); // createFromArray generate exception if array for Complex not from two elements
            }
        }
        return cM;
    }

    public MathMatrix add(MathMatrix n) {
        if (row != n.getRow() || col != n.getCol()) {
            throw new DiffDimension(this, n);
        }

        MathMatrix s = new MathMatrix(type.copy(), row, col);

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                s.setData(i, j, m[i][j].add(n.getData(i, j)));
            }
        }
        return s;
    }

    public MathMatrix multiply(MathMatrix n) {
        if (n.getRow() == 1 && n.getCol() == 1) {
            return this.multiply(n.getData(0, 0));
        } else if (row == 1 && col == 1) {
            return n.multiply(getData(0, 0));
        }

        if (col != n.getRow()) {
            throw new IncompatibleDimension(this, n);
        }

        MathMatrix s = new MathMatrix(type.copy(), row, n.getCol());

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < n.getCol(); j++) {
                IMath tmp = type.copy();
                for (int k = 0; k < col; k++) {
                    tmp = tmp.add(m[i][k].multiply(n.getData(k, j)));
                }
                s.setData(i, j, tmp);
            }
        }
        return s;
    }

    public MathMatrix multiply(IMath n) {

        MathMatrix s = new MathMatrix(type.copy(), row, col);

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                s.setData(i, j, m[i][j].multiply(n));
            }
        }
        return s;
    }

    public boolean equals(Object n) {
        if (n == null) return false;
        if (n == this) return true;
        if (!(n instanceof MathMatrix)) return false;
        MathMatrix mm = (MathMatrix) n;
        if (mm.getRow() != row || mm.getCol() != col) return false;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (!m[i][j].equals(mm.getData(i, j))) return false;
            }
        }
        return true;
    }
}
