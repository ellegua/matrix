package com.company;

import com.company.Exceptions.NullArray;

import java.util.Scanner;

public class MathComplex implements IMath {
    private double re; // TODO Define class MathDouble
    private double im;

    public double getRe() {
        return re;
    }

    public double getIm() {
        return im;
    }

    public MathComplex(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public MathComplex(double re) {
        this(re, 0);
    }

    public MathComplex() {
        this(0, 0);
    }

    public MathComplex createFromConsole() {//todo think about catch right here or special class for console input
        Scanner in = new Scanner(System.in);
        return new MathComplex(in.nextDouble(), in.nextDouble());//add throw exception if not double is input
    }

    public String toString() {
        return re + "+i" + im;
    }

    public MathComplex copy() {
        return new MathComplex(re, im);
    }

    public MathComplex createFromArray(double staff[]) {
        if (staff.length < 2) {
            throw new NullArray(); // need test
        }
        return new MathComplex(staff[0], staff[1]);
    }

/*    IMath createFromArray(IMath staff[]){
        if (staff.length < 2) {
            throw new NullArray(); // need test
        }
        return new MathComplex((MathInt)staff[0], (MathInt)staff[1]);
    }*/


    public MathComplex add(IMath val) {//todo write test
        return new MathComplex(re + ((MathComplex) val).getRe(), im + ((MathComplex) val).getIm());
    }

    public MathComplex multiply(IMath val) {//todo write test
        return new MathComplex(re * ((MathComplex) val).getRe() - im * ((MathComplex) val).getIm(), re * ((MathComplex) val).getIm() + im * ((MathComplex) val).getRe());
    }

    public boolean equals(Object n) {//todo write test
        if (n == null) return false;
        if (n == this) return true;
        if (!(n instanceof MathComplex)) return false;
        //MathComplex n = (MathComplex) n;
        if (((MathComplex) n).getRe() != re || ((MathComplex) n).getIm() != im) return false;
        return true;
    }
    //I don't re-define hashTag method because it is redundant at this moment and to not produce extra warnings
}
