package com.company;

import com.company.Exceptions.NullArray;

import java.util.Scanner;

public class MathInt implements IMath {

    private int i;

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    MathInt(int i) {
        this.i = i;
    }

    MathInt() {
        this(0);
    }

    MathInt(double d) {
        this((int) d);
    }

    public String toString() {
        return String.format("%s", i);
    }

    public IMath copy() {
        return new MathInt(i);
    }

    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (!(o instanceof MathInt)) return false;
        if (((MathInt) o).getI() != i) return false;
        return true;
    }

    public IMath add(IMath val) {
        return new MathInt(i + ((MathInt) val).getI());
    }

    public IMath multiply(IMath val) {
        return new MathInt(i * ((MathInt) val).getI());
    }

    public MathInt createFromConsole() {
        Scanner in = new Scanner(System.in);//todo use big Double to convert from string
        return new MathInt(in.nextInt());//add throw exception if not double is input
    }

    public MathInt createFromArray(double staff[]) {
        if (staff.length < 1) {
            throw new NullArray(); // need test
        }
        return new MathInt((int) staff[0]); // todo should be temporary
    }
}
