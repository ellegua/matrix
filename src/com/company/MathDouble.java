package com.company;

import com.company.Exceptions.NullArray;

import java.util.Scanner;

public class MathDouble implements IMath {

    private double d;

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    MathDouble(double d) {
        this.d = d;
    }

    MathDouble() {
        this(0e0);
    }

    public String toString(){

        return String.format("%s",d);
    }

    public IMath copy(){
        return new MathDouble(d);
    }

    public boolean equals(Object o){
        if (o == null) return false;
        if (o == this) return true;
        if (!(o instanceof MathDouble)) return false;
        if (((MathDouble) o).getD()!= d ) return false;
        return true;
    }

    public IMath add(IMath val){
        return new MathDouble(d + ((MathDouble) val).getD());
    }

    public IMath multiply(IMath val){
        return new MathDouble(d * ((MathDouble) val).getD());
    }

    public MathDouble createFromConsole(){
        Scanner in = new Scanner(System.in);//todo use big Double to convert from string
        return new MathDouble(in.nextDouble());//add throw exception if not double is input
    }

    public MathDouble createFromArray(double staff[]) {// todo should be temporary
        if (staff.length < 1) {
            throw new NullArray(); // need test
        }
        return new MathDouble(staff[0]);
    }
}
