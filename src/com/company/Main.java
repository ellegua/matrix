package com.company;

import com.company.Exceptions.*;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //testMathDouble();
        //testMathInt();


        try {
            // TODO Use separated try or define special test class
            // Test block
            Main.test_WhenItIsCreatingEmptyMathMatrix_ItGeneratesNullDimensionException();

            int dimensionData[][] = {{-1, 0}, {0, -1}, {1, 0}, {0, 1}};
            for (int i = 0; i < dimensionData.length; i++) {
                Main.test_WhenItIsSettingDataWithRowOrColOutOfDimension_ItGeneratesOutOfDimensionException(dimensionData[i]);
                Main.test_WhenItIsGettingDataWithRowOrColOutOfDimension_ItGeneratesOutOfDimensionException(dimensionData[i]);
            }

            double[][][] staff0 = {};
            Main.test_WhenItIsCreatingFromWrongArray_ItGeneratesNullArrayException(staff0);
            double[][][] staff1 = {{}};
            Main.test_WhenItIsCreatingFromWrongArray_ItGeneratesNullArrayException(staff1);

            test_WhenItIsAddingMathMatrixWithDiffDimension_ItGeneratesDiffDimensionException();

            MathMatrix m1 = new MathMatrix(new MathComplex(), 1, 2);
            MathMatrix m2 = new MathMatrix(new MathComplex(), 1, 2);
            test_WhenItIsMultiplyingMathMatrixWithIncompatibleDimension_ItGeneratesIncompatibleDimensionException(m1, m2);

            test_methodEqualsForMathComplex();
            //test_methodEqualsForObject(new MathComplex());
            //test_methodEqualsForObject(new MathMatrix()); // TODO only after MathMatrix implement IMath
            test_WhenItIsAddingMathComplex_ItGeneratesTestFailedException();
            test_WhenItIsMultiplyingMathComplex_ItGeneratesTestFailedException();

            test_methodEqualsForMathMatrix();
            test_WhenItIsAddingMathMatrix_ItGeneratesTestFailedException();
            test_WhenItIsMultiplyingMathMatrix_ItGeneratesTestFailedException();

            //test();
            // End of test block

            consoleLoopUserInterfaceWithMathMatrix();

        } catch (TestFailed e) {
            System.out.println(e.toString());
            System.out.println("MathMatrix is NOT ready.");
        } catch (RuntimeException e) {
            System.out.println("I have caught RuntimeException");
            System.out.println(e.toString());
        } catch (Exception e) {
            System.out.println("I have caught Exception");
            System.out.println(e.toString());
        } catch (Error e) {
            System.out.println("I have caught Error");
            System.out.println(e.toString());
        } finally {
            System.out.println("See you again!");
        }
    }

    //todo make test independent from double and MathComplex types

    public static String getMethodName() {
        return Thread.currentThread().getStackTrace()[1].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName();
    }

    public static void test_WhenItIsCreatingEmptyMathMatrix_ItGeneratesNullDimensionException() {
        boolean pass = false;
        String msg = getMethodName() + "\t\t";
        try {
            new MathMatrix(new MathComplex(), 0, 0);
        } catch (NullDimension e) {
            pass = true;
            System.out.println(new TestPassed(msg).toString());
        } finally {
            if (pass == false) {
                throw new TestFailed(getMethodName() + "\t\t");
            }
        }
    }

    public static void test_WhenItIsSettingDataWithRowOrColOutOfDimension_ItGeneratesOutOfDimensionException(int dimension[]) {
        boolean pass = false;
        String msg = getMethodName() + " {" + dimension[0] + "," + dimension[1] + "}" + "\t\t";
        try {
            MathMatrix m = new MathMatrix(new MathComplex(), 1, 1);
            m.setData(dimension[0], dimension[1], new MathComplex());
        } catch (OutOfDimension e) {
            pass = true;
            System.out.println(new TestPassed(msg).toString());
        } finally {
            if (pass == false) {
                throw new TestFailed(msg);
            }
        }
    }

    public static void test_WhenItIsGettingDataWithRowOrColOutOfDimension_ItGeneratesOutOfDimensionException(int dimension[]) {
        boolean pass = false;
        String msg = getMethodName() + " {" + dimension[0] + "," + dimension[1] + "}" + "\t\t";

        try {
            MathMatrix m = new MathMatrix(new MathComplex());
            m.getData(dimension[0], dimension[1]);
        } catch (OutOfDimension e) {
            pass = true;
            System.out.println(new TestPassed(msg).toString());
        } finally {
            if (pass == false) {
                throw new TestFailed(msg);
            }
        }
    }

    public static void test_WhenItIsCreatingFromWrongArray_ItGeneratesNullArrayException(double staff[][][]) {
        boolean pass = false;
        String msg = getMethodName() + "\t\t" + Arrays.deepToString(staff);
        try {
            MathMatrix.createFromArray(new MathComplex(), staff);
        } catch (NullArray e) {
            pass = true;
            System.out.println(new TestPassed(msg).toString());
        } finally {
            if (pass == false) {
                throw new TestFailed(msg);
            }
        }
    }

    public static void test_WhenItIsAddingMathMatrixWithDiffDimension_ItGeneratesDiffDimensionException() {
        boolean pass = false;
        String msg = getMethodName() + "\t\t";
        try {
            MathMatrix m1 = new MathMatrix(new MathComplex(), 1, 1);
            MathMatrix m2 = new MathMatrix(new MathComplex(), 1, 2);
            m1.add(m2);
        } catch (DiffDimension e) {
            pass = true;
            System.out.println(new TestPassed(msg).toString());
        } finally {
            if (pass == false) {
                throw new TestFailed(msg);
            }
        }
    }

    public static void test_WhenItIsMultiplyingMathMatrixWithIncompatibleDimension_ItGeneratesIncompatibleDimensionException(MathMatrix m1, MathMatrix m2) {
        boolean pass = false;
        String msg = getMethodName() + "\t\t";
        try {
            m1.multiply(m2);
        } catch (IncompatibleDimension e) {
            pass = true;
            System.out.println(new TestPassed(msg).toString());
        } finally {
            if (pass == false) {
                throw new TestFailed(msg);
            }
        }
    }

    //todo ask can it be?
/*    public static void test_methodEqualsForObject(IMath o) {
        String msg = getMethodName() + "\t\t";
        if (o.equals(null)) throw new TestFailed(msg);
        if (!o.equals(o)) throw new TestFailed(msg);
        IMath oCopy=o.copy(); Object ob = new Object(); Object ob2=ob.clone(); //java: clone() has protected access in java.lang.Object
        if (o.equals(oCopy) && !oCopy.equals(o)) throw new TestFailed(msg);
        Object oCopyToTest = oCopy.copy();
        if (o.equals(oCopy) && oCopy.equals(oCopyToTest) && !o.equals(oCopyToTest)) throw new TestFailed(msg);
        System.out.println(new TestPassed(msg).toString());
    }*/

    public static void test_methodEqualsForMathComplex() {
        String msg = getMethodName() + "\t\t";
        MathComplex m1 = new MathComplex();
        if (m1.equals(null)) throw new TestFailed(msg);
        if (!m1.equals(m1)) throw new TestFailed(msg);
        MathComplex m2 = new MathComplex();
        if (m1.equals(m2) && !m2.equals(m1)) throw new TestFailed(msg);
        MathComplex m3 = new MathComplex();
        if (m1.equals(m2) && m2.equals(m3) && !m1.equals(m3)) throw new TestFailed(msg);
        System.out.println(new TestPassed(msg).toString());
    }

    public static void test_WhenItIsAddingMathComplex_ItGeneratesTestFailedException() {
        String msg = getMethodName() + "\t\t";
        MathComplex m1 = new MathComplex();
        //System.out.println(m1);
        MathComplex m2 = new MathComplex();
        //System.out.println(m2);
        //System.out.println(m1.add(m2));
        MathComplex m3 = new MathComplex();
        //System.out.println(m3);
        if (!m1.add(m2).equals(m3)) throw new TestFailed(msg);
        System.out.println(new TestPassed(msg).toString());
    }

    public static void test_WhenItIsMultiplyingMathComplex_ItGeneratesTestFailedException() {
        String msg = getMethodName() + "\t\t";
        MathComplex m1 = new MathComplex();
        //System.out.println(m1);
        MathComplex m2 = new MathComplex();
        //System.out.println(m2);
        //System.out.println(m1.multiply(m2));
        MathComplex m3 = new MathComplex();
        //System.out.println(m3);
        if (!m1.multiply(m2).equals(m3)) throw new TestFailed(msg);
        System.out.println(new TestPassed(msg).toString());
    }

    public static void test_methodEqualsForMathMatrix() {
        String msg = getMethodName() + "\t\t";
        MathMatrix m1 = new MathMatrix(new MathComplex(), 1, 1);
        if (m1.equals(null)) throw new TestFailed(msg);
        if (!m1.equals(m1)) throw new TestFailed(msg);
        MathMatrix m2 = new MathMatrix(new MathComplex(), 1, 1);
        if (m1.equals(m2) && !m2.equals(m1)) throw new TestFailed(msg);
        MathMatrix m3 = new MathMatrix(new MathComplex(), 1, 1);
        if (m1.equals(m2) && m2.equals(m3) && !m1.equals(m3)) throw new TestFailed(msg);
        System.out.println(new TestPassed(msg).toString());
    }

    public static void test_WhenItIsAddingMathMatrix_ItGeneratesTestFailedException() {
        String msg = getMethodName() + "\t\t";
        MathMatrix m1 = new MathMatrix(new MathComplex(), 1, 1);
        //System.out.println(m1);
        MathMatrix m2 = new MathMatrix(new MathComplex(), 1, 1);
        //System.out.println(m2);
        //System.out.println(m1.add(m2));
        MathMatrix m3 = new MathMatrix(new MathComplex(), 1, 1);
        //System.out.println(m3);
        if (!m1.add(m2).equals(m3)) throw new TestFailed(msg);
        System.out.println(new TestPassed(msg).toString());
    }

    public static void test_WhenItIsMultiplyingMathMatrix_ItGeneratesTestFailedException() {
        String msg = getMethodName() + "\t\t";
        MathMatrix m1 = new MathMatrix(new MathComplex(), 1, 1);
        //System.out.println(m1);
        MathMatrix m2 = new MathMatrix(new MathComplex(), 1, 1);
        //System.out.println(m2);
        //System.out.println(m1.multiply(m2));
        MathMatrix m3 = new MathMatrix(new MathComplex(), 1, 1);
        //System.out.println(m3);
        if (!m1.multiply(m2).equals(m3)) throw new TestFailed(msg);
        System.out.println(new TestPassed(msg).toString());
    }

    public static void test() {
        throw new AssertionError(true);
    }

    public static void testMathDouble() {
        MathDouble d = new MathDouble(1.68);
        System.out.println(d.toString());
        System.out.println(d.add(new MathDouble(3.14)).toString());
        System.out.println(d.multiply(new MathDouble(3.14)).toString());
        MathDouble cd = d.createFromConsole();
        System.out.println(cd.toString());
    }

    public static void testMathInt() {
        MathInt d = new MathInt(1.68);
        System.out.println(d.toString());
        System.out.println(d.add(new MathInt(3.14)).toString());
        System.out.println(d.multiply(new MathInt(3.14)).toString());
        MathInt cd = d.createFromConsole();
        System.out.println(cd.toString());
    }

    public static void consoleLoopUserInterfaceWithMathMatrix() {

        System.out.println("MathMatrix is ready.");//only if all test passed
        System.out.println("MathMatrix User Interface Welcome You!");

        boolean again;
        do {
            try {
                again = consoleUserInterfaceWithMathMatrix();
                //again=false;
            } catch (RuntimeException e) {
                System.out.println("I have got RuntimeException");
                System.out.println(e.toString());
                again = true;
            } catch (OutOfMemoryError e) {
                System.out.println(e.toString());
                System.out.println("Congratulation! You have got the second chance! Please try again.");
                again = true;
            }
        } while (again);
    }

    public static boolean consoleUserInterfaceWithMathMatrix() {
        int c;
        //do {
        wouldYouLikeToContinue();
        Scanner in_c = new Scanner(System.in);
        c = in_c.nextInt();
        start:
        switch (c) {
            case 1:
                System.out.println("Ok. We are continuing.");
                int t;
                do {
                    WhatTypeOfMathMatrixWouldYouLike();
                    Scanner in_t = new Scanner(System.in);
                    t = in_t.nextInt();
                    switch (t) {
                        case 0:
                            System.out.println("Ok. You have canceled.");
                            break;
                        case 1:
                            System.out.println("Your choose is MathComplex!");
                            System.out.println("Note: MathComplex input format is two double separated by a space like: 0 0"); // hard-code
                            int n;
                            do {
                                WhatWouldYouLikeToDo();
                                Scanner in_n = new Scanner(System.in);
                                n = in_n.nextInt();
                                switch (n) {
                                    case 0:
                                        System.out.println("Ok. You have canceled.");
                                        break start;
                                    case 1:
                                        System.out.println("Your choose is Add!");
                                        addMathMatrixFromMainConsole(new MathComplex());
                                        break start;
                                    case 2:
                                        System.out.println("Your choose is Multiply!");
                                        multiplyMathMatrixFromMainConsole(new MathComplex());
                                        break start;
                                    default:
                                        System.out.println("Congratulation! You have got the second chance! Please try again.");
                                }
                            } while (n != 0);
                            break;
                        case 2:
                            System.out.println("Your choose is MathInt!");
                            int n2;
                            do {
                                WhatWouldYouLikeToDo();
                                Scanner in_n2 = new Scanner(System.in);
                                n2 = in_n2.nextInt();
                                switch (n2) {
                                    case 0:
                                        System.out.println("Ok. You have canceled.");
                                        break start;
                                    case 1:
                                        System.out.println("Your choose is Add!");
                                        addMathMatrixFromMainConsole(new MathInt());
                                        break start;
                                    case 2:
                                        System.out.println("Your choose is Multiply!");
                                        multiplyMathMatrixFromMainConsole(new MathInt());
                                        break start;
                                    default:
                                        System.out.println("Congratulation! You have got the second chance! Please try again.");
                                }
                            } while (n2 != 0);
                            break;
                        case 3:
                            System.out.println("Your choose is MathDouble!");
                            int n3;
                            do {
                                WhatWouldYouLikeToDo();
                                Scanner in_n3 = new Scanner(System.in);
                                n3 = in_n3.nextInt();
                                switch (n3) {
                                    case 0:
                                        System.out.println("Ok. You have canceled.");
                                        break start;
                                    case 1:
                                        System.out.println("Your choose is Add!");
                                        addMathMatrixFromMainConsole(new MathDouble());
                                        break start;
                                    case 2:
                                        System.out.println("Your choose is Multiply!");
                                        multiplyMathMatrixFromMainConsole(new MathDouble());
                                        break start;
                                    default:
                                        System.out.println("Congratulation! You have got the second chance! Please try again.");
                                }
                            } while (n3 != 0);
                            break;
                        default:
                            System.out.println("Congratulation! You have got the second chance! Please try again.");
                    }
                } while (t != 0);
                break;
            case 0:
                System.out.println("Ok. You have wished to exit.");
                return false;
            //break;
            default:
                System.out.println("Congratulation! You have got the second chance! Please try again.");
        }
        //} while (c != 0);
        return true;
    }

    public static void wouldYouLikeToContinue() {
        System.out.println("Would you like to continue?");
        System.out.println("[1] to continue");
        System.out.println("[0] to exit");
        System.out.println("Please do your choose and enter the number:");
    }

    public static void WhatTypeOfMathMatrixWouldYouLike() {
        System.out.println("What Type of MathMatrix would You like?");
        System.out.println("The following Types are available:");
        System.out.println("[1] MathComplex");
        System.out.println("[2] MathInt");
        System.out.println("[3] MathDouble");
        System.out.println("[0] to cancel");
        System.out.println("Please choose the type and enter the number:");
    }


    public static void WhatWouldYouLikeToDo() {
        System.out.println("What would you like to do?");
        System.out.println("The following operations are available:");
        System.out.println("[1] Add");
        System.out.println("[2] Multiply");
        System.out.println("[0] to cancel");
        System.out.println("Please choose the operation and enter the number:"); // while before number or press smth to out

    }


    public static void multiplyMathMatrixFromMainConsole(IMath type) {
        MathMatrix cm1 = createMathMatrixFromMainConsole(type, "1st");
        MathMatrix cm2 = createMathMatrixFromMainConsole(type, "2nd");// return exception after wrong row or col and before input data
        try {
            MathMatrix cm1_m2 = cm1.multiply(cm2);
            System.out.println("The result of the chosen operation:");
            System.out.println(cm1_m2.toString());
        } catch (RuntimeException e) {
            System.out.println(e.toString());
        }
    }

    public static void addMathMatrixFromMainConsole(IMath type) {
        MathMatrix cm1 = createMathMatrixFromMainConsole(type, "1st");
        MathMatrix cm2 = createMathMatrixFromMainConsole(type, "2nd");
        try {
            MathMatrix cm1_m2 = cm1.add(cm2);
            System.out.println("The result of the chosen operation:");
            System.out.println(cm1_m2.toString());
        } catch (RuntimeException e) {
            System.out.println(e.toString());
        }
    }

    public static MathMatrix createMathMatrixFromMainConsole(IMath type, String msg) {
        MathMatrix cm = null;
        do {
            try {
                System.out.println(msg);
                System.out.println("Please enter the MathMatrix:"); // while before number or press smth to out
                cm = MathMatrix.createFromConsole(type);
                System.out.println("You have entered the following MathMatrix:");
                System.out.println(cm.toString());

            } catch (RuntimeException e) {
                System.out.println(e.toString());
            }
        } while (cm == null);
        return cm;
    }

}
// TODO create doc comments