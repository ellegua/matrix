package com.company.Exceptions;

import com.company.MathMatrix;

public class DiffDimension extends RuntimeException{
    private MathMatrix m1;
    private MathMatrix m2;

    public DiffDimension(MathMatrix m1, MathMatrix m2){
        this.m1=m1;
        this.m2=m2;
    }

    public String getMessage() {
        return "Different Matrix Dimensions: First Matrix" + "row=" + m1.getRow() + "," + " col=" + m1.getCol() + ";" + " Second Matrix: " + "row=" + m2.getRow() + "," + " col=" + m2.getCol() + ".";
    }
}
