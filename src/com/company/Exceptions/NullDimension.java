package com.company.Exceptions;

public class NullDimension extends RuntimeException {
    private int row;
    private int col;

    public NullDimension(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public String getMessage() {
        return "Incorrect Matrix Dimension. row or/and col cannot be zero or negative! " + "row=[" + row + "]" + " col=[" + col + "]";
    }

}
