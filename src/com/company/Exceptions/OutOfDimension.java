package com.company.Exceptions;

import com.company.MathMatrix;

public class OutOfDimension extends RuntimeException {
    private int row;
    private int col;
    private MathMatrix m;

    public OutOfDimension(int row, int col, MathMatrix m) {
        this.row = row;
        this.col = col;
        this.m = m;
    }

    public String getMessage() {
        return "Incorrect Matrix Dimension:" + "row=" + row + "," + " col=" + col + "." + " row or/and col cannot be out of the dimension: " + "row=" + m.getRow() + "," + " col=" + m.getCol() + ".";
    }

}
