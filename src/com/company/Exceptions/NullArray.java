package com.company.Exceptions;

public class NullArray extends RuntimeException {
    public String getMessage() {
        return "Null Array. Array have to have at least one element.";
    }
}
