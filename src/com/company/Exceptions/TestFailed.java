package com.company.Exceptions;

public class TestFailed extends RuntimeException {
    private String msg;

    public TestFailed(String msg) {
        this.msg = msg;
    }

    public String getMessage() {
        return "[FAILED] "+ msg;
    }

    public String toString(){ // to output without exception type
        return getMessage();
    }
}
