package com.company;

public interface IMath {

    String toString();

    IMath copy();//todo think and read about re-define clone

    boolean equals(Object n);

    IMath add(IMath val);

    IMath multiply(IMath val);

    IMath createFromConsole();

    IMath createFromArray(double staff[]);// todo should be temporary

    //IMath createFromArray(IMath staff[]);


}
